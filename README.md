# Statically yours

This module creates S3 and Cloudflare resources necessary to deploy a static site. A [GitLab CI YAML](gitlab-ci.yml) is also included that demonstrates how to automate deployment.

To make the concrete instances of this module work, I'm setting the following environment variables:

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
TF_VAR_site_domain
TF_VAR_site_staging_subdomain
TF_VAR_site_staging_domain
TF_VAR_cloudflare_email
TF_VAR_cloudflare_api_key
```

You don't have to use environment variables, it's all the [usual tfvar stuff](https://www.terraform.io/docs/configuration/variables.html) but that's what I did for my case.

I set `aws_region` in the invoker of the module.

Here's an example `main.tf` from a site of mine that uses this module:

```hcl
provider "aws" {
  version = "~> 2.0"
  region  = var.aws_region
}

provider "cloudflare" {
  version = "~> 2.0"
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "site_domain" {
  type = string
}

variable "site_staging_subdomain" {
  type = string
  default = "staging"
}

variable "site_staging_domain" {
  type = string
}

variable "cloudflare_email" {
  type = string
}

variable "cloudflare_api_key" {
  type = string
}

# name the module instance with something that tells you what the resources
# are for. In my case, I used the module name then a suffix of which site
# the module resources are for.
# Don't forget to update the SHA1 ref for your use-case. Don't use unqualified
# module sources! Always set the hash!
module "statically_yours_mysite" {
  source = "git::https://gitlab.com/bitemyapp/statically-yours.git?ref=11a69157689ca3881d4d2d8eda5a990c01fc1a1a"

  aws_region = "us-east-1"
  site_domain = var.site_domain
  site_staging_subdomain = var.site_staging_subdomain
  site_staging_domain = var.site_staging_domain
  cloudflare_email = var.cloudflare_email
  cloudflare_api_key = var.cloudflare_api_key
}
```

I set my Terraform variables using an `env.sh` that I `source` in zsh to set the environment variables. Before you ask, I don't know why I set `aws_region = "us-east-1"` instead of using `var.aws_region`. Try it t'other way around and report back if it works.
